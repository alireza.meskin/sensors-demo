package com.example.sensors.domain;

import com.example.sensors.domain.models.Measurement;
import com.example.sensors.domain.models.Sensor;
import com.example.sensors.domain.models.Sensor.Status;

public interface SensorStatusHandlerInterface {

  Status newMeasurement(Sensor sensor, Measurement measurement);

}
