package com.example.sensors.domain;

import com.example.sensors.domain.exceptions.SensorNotFoundException;
import com.example.sensors.domain.models.Alert;
import com.example.sensors.domain.models.Measurement;
import com.example.sensors.domain.models.Sensor;
import com.example.sensors.domain.models.Sensor.Status;
import com.example.sensors.domain.repositories.AlertRepository;
import com.example.sensors.domain.repositories.MeasurementRepository;
import com.example.sensors.domain.repositories.SensorRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.example.sensors.domain.models.Sensor.Status.ALERT;

public class SensorService {

  private final AlertRepository alertRepository;
  private final SensorRepository sensorRepository;
  private final MeasurementRepository measurementRepository;
  private final SensorStatusHandlerInterface statusHandler;

  public SensorService(
          SensorStatusHandlerInterface statusHandler,
          SensorRepository sensorRepository,
          AlertRepository alertRepository,
          MeasurementRepository measurementRepository
  ) {
    this.statusHandler = statusHandler;
    this.sensorRepository = sensorRepository;
    this.alertRepository = alertRepository;
    this.measurementRepository = measurementRepository;
  }

  public Status getSensorStatus(UUID uuid) {
    Sensor sensor = sensorRepository.findById(uuid).orElseThrow(() -> new SensorNotFoundException(uuid));
    return sensor.getStatus();
  }

  public Optional<Integer> getMaximumLevelInLastDays(UUID sensorId, Integer days) {
    LocalDateTime firstDate = LocalDate.now().minusDays(days).atStartOfDay();
    LocalDateTime lastDate  = LocalDate.now().atTime(LocalTime.MAX);

    return measurementRepository.findMaxBetweenDates(sensorId, firstDate, lastDate);
  }

  public Optional<Double> getAverageLevelInLastDays(UUID sensorId, Integer days) {
    LocalDateTime firstDate = LocalDate.now().minusDays(days).atStartOfDay();
    LocalDateTime lastDate  = LocalDate.now().atTime(LocalTime.MAX);

    return measurementRepository.findAverageBetweenDates(sensorId, firstDate, lastDate);
  }

  public List<Alert> getSensorAlerts(UUID sensorId) {
    Sensor sensor = sensorRepository.findById(sensorId).orElseThrow(() -> new SensorNotFoundException(sensorId));

    return alertRepository.findBySensorId(sensor.getUuid());
  }

  public void submitMeasurement(UUID uuid, Integer co2, LocalDateTime time) {
    Sensor sensor           = sensorRepository.findById(uuid).orElseThrow(() -> new SensorNotFoundException(uuid));
    Measurement measurement = measurementRepository.save(new Measurement(uuid, co2, time));

    Status oldStatus = sensor.getStatus();
    Status newStatus = statusHandler.newMeasurement(sensor, measurement);

    processMeasurementForAlerts(measurement, oldStatus, newStatus);

    if (newStatus != sensor.getStatus()) {
      sensor.setStatus(newStatus);

      sensorRepository.save(sensor);
    }
  }

  private void processMeasurementForAlerts(Measurement measurement, Status oldStatus, Status newStatus) {
    if (oldStatus != ALERT && newStatus == ALERT) {
      Alert alert = new Alert();
      alert.setStartTime(LocalDateTime.now());
      alert.setSensorId(measurement.getSensorId());
      alert.setFinished(false);

      alertRepository.save(alert);
    }

    if (oldStatus == ALERT && newStatus != ALERT) {
      Optional<Alert> alert = alertRepository.findUnfinishedAlert(measurement.getSensorId());

      alert.ifPresent(a -> {
        a.setFinished(true);
        a.setEndTime(measurement.getTime());

        alertRepository.save(a);
      });
    }
  }
}
