package com.example.sensors.domain.exceptions;

import java.util.UUID;

public class SensorNotFoundException extends RuntimeException {
  public SensorNotFoundException(UUID uuid) {
    super("Could not find sensor " + uuid);
  }
}
