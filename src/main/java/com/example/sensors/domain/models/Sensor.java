package com.example.sensors.domain.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import java.util.UUID;


@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Sensor {
  @Id
  private UUID uuid;

  @Enumerated(EnumType.STRING)
  private Status status;

  public enum Status {
    OK,
    WARN,
    ALERT
  }
}
