package com.example.sensors.domain.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;


@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Measurement {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  private UUID sensorId;

  private Integer co2;

  private LocalDateTime time;

  /**
   * Constructs without ID.
   */
  public Measurement(UUID sensorId, Integer co2, LocalDateTime time) {
    this.sensorId = sensorId;
    this.co2 = co2;
    this.time = time;
  }
}
