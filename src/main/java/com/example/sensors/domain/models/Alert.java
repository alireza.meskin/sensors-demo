package com.example.sensors.domain.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Alert {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  private UUID sensorId;

  private LocalDateTime startTime;

  private LocalDateTime endTime;

  private Boolean finished;
}
