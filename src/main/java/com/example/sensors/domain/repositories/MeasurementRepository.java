package com.example.sensors.domain.repositories;

import com.example.sensors.domain.models.Measurement;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

public interface MeasurementRepository {
  Measurement save(Measurement entity);

  Optional<Integer> findMaxBetweenDates(UUID sensorId, LocalDateTime firstDate, LocalDateTime lastDate);

  Optional<Double> findAverageBetweenDates(UUID sensorId, LocalDateTime firstDate, LocalDateTime lastDate);
}
