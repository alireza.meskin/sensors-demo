package com.example.sensors.domain.repositories;

import com.example.sensors.domain.models.Sensor;

import java.util.Optional;
import java.util.UUID;

public interface SensorRepository {
  Optional<Sensor> findById(UUID uuid);

  Sensor save(Sensor sensor);
}
