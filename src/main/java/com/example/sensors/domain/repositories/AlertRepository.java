package com.example.sensors.domain.repositories;

import com.example.sensors.domain.models.Alert;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface AlertRepository {
  Alert save(Alert alert);

  List<Alert> findBySensorId(UUID sensorId);

  Optional<Alert> findUnfinishedAlert(UUID sensorId);
}
