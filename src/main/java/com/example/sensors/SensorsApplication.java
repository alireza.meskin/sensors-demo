package com.example.sensors;

import com.example.sensors.domain.models.Sensor;
import com.example.sensors.domain.repositories.SensorRepository;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.annotation.EnableJms;

import java.time.LocalDateTime;
import java.util.UUID;

@SpringBootApplication
@EnableJms
public class SensorsApplication {

  public static void main(String[] args) {
    SpringApplication.run(SensorsApplication.class, args);
  }

  @Bean
  public InitializingBean seedDatabase(SensorRepository sensorRepository) {
    return () -> {
      System.out.println("sensorRepository = " + LocalDateTime.now().toString());
      sensorRepository.save(new Sensor(UUID.fromString("58cc88f7-a824-4f2d-97ba-0715b1a302f1"), Sensor.Status.OK));
      sensorRepository.save(new Sensor(UUID.fromString("7dc34228-4e8c-43d7-99b9-9c63dd323534"), Sensor.Status.OK));
      sensorRepository.save(new Sensor(UUID.fromString("78d90d8b-bfbb-4e47-9ad2-2ad60d2a9208"), Sensor.Status.OK));
    };
  }

}
