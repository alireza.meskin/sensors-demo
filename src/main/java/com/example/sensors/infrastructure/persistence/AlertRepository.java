package com.example.sensors.infrastructure.persistence;

import com.example.sensors.domain.models.Alert;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface AlertRepository extends
        com.example.sensors.domain.repositories.AlertRepository, JpaRepository<Alert, Long> {

  @Query("SELECT a FROM Alert a WHERE a.sensorId = :sensorId AND a.finished = false")
  Optional<Alert> findUnfinishedAlert(UUID sensorId);
}
