package com.example.sensors.infrastructure.persistence;

import com.example.sensors.domain.models.Sensor;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface SensorRepository extends
        com.example.sensors.domain.repositories.SensorRepository,
        JpaRepository<Sensor, UUID> {
}

