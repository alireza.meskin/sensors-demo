package com.example.sensors.infrastructure.persistence;

import com.example.sensors.domain.models.Measurement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface MeasurementRepository extends
        com.example.sensors.domain.repositories.MeasurementRepository,
        JpaRepository<Measurement, Long> {

  @Query("SELECT MAX(m.co2) FROM Measurement m WHERE m.sensorId = :sensorId AND m.time >= :firstDate AND m.time <= :lastDate")
  Optional<Integer> findMaxBetweenDates(UUID sensorId, LocalDateTime firstDate, LocalDateTime lastDate);

  @Query("SELECT AVG(m.co2) FROM Measurement m WHERE m.sensorId = :sensorId AND m.time >= :firstDate AND m.time <= :lastDate")
  Optional<Double> findAverageBetweenDates(UUID sensorId, LocalDateTime firstDate, LocalDateTime lastDate);
}
