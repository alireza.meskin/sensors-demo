package com.example.sensors.infrastructure.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
public class AlertDto {

  private LocalDateTime startTime;

  private LocalDateTime endTime;
}
