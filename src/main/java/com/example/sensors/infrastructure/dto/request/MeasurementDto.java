package com.example.sensors.infrastructure.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
public class MeasurementDto {
  private Integer co2;

  private LocalDateTime time;
}
