package com.example.sensors.infrastructure.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class MetricDto {

  private Integer maxLast30Days;

  private Double averageLast30Days;
}

