package com.example.sensors.infrastructure.controllers;

import com.example.sensors.domain.SensorService;
import com.example.sensors.domain.models.Alert;
import com.example.sensors.domain.models.Sensor;
import com.example.sensors.infrastructure.dto.request.MeasurementDto;
import com.example.sensors.infrastructure.dto.response.AlertDto;
import com.example.sensors.infrastructure.dto.response.MetricDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;


@RestController
public class SensorController {

  @Autowired
  private SensorService sensorService;

  @GetMapping("/api/v1/sensors/{uuid}")
  public Map<String, String> getSensorStatus(@PathVariable UUID uuid) throws Exception {
    Sensor.Status status = sensorService.getSensorStatus(uuid);

    return Collections.singletonMap("status", status.toString());
  }

  @GetMapping("/api/v1/sensors/{uuid}/metrics")
  public MetricDto getSensorMetrics(@PathVariable UUID uuid) throws Exception {
    Double average = sensorService.getAverageLevelInLastDays(uuid, 30).orElse(0.0);
    Integer maximum = sensorService.getMaximumLevelInLastDays(uuid, 30).orElse(0);

    return new MetricDto(maximum, average);
  }

  @PostMapping("/api/v1/sensors/{uuid}/measurements")
  public Map<String, String> submitMeasurement(@PathVariable UUID uuid, @RequestBody MeasurementDto requestDto) {
    sensorService.submitMeasurement(uuid, requestDto.getCo2(), requestDto.getTime());

    return Collections.singletonMap("status", "Accepted");
  }

  @GetMapping("/api/v1/sensors/{uuid}/alerts")
  public List<AlertDto> getSensorAlerts(@PathVariable UUID uuid) {
    List<Alert> alerts = sensorService.getSensorAlerts(uuid);

    return alerts
        .stream()
        .map(e -> new AlertDto(e.getStartTime(), e.getEndTime()))
        .collect(Collectors.toList());
  }
}
