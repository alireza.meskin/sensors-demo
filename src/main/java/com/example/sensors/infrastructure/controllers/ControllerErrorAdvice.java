package com.example.sensors.infrastructure.controllers;

import com.example.sensors.domain.exceptions.SensorNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ControllerErrorAdvice {

  @ResponseBody
  @ExceptionHandler(SensorNotFoundException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  String sensorNotFoundHandler(SensorNotFoundException exception) {
    return exception.getMessage();
  }

  @ResponseBody
  @ExceptionHandler(IllegalArgumentException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  String invalidUuidHandler(IllegalArgumentException exception) {
    return exception.getMessage();
  }
}
