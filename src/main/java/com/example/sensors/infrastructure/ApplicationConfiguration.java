package com.example.sensors.infrastructure;

import com.example.sensors.domain.SensorService;
import com.example.sensors.domain.SensorStatusHandlerInterface;
import com.example.sensors.domain.repositories.AlertRepository;
import com.example.sensors.domain.repositories.MeasurementRepository;
import com.example.sensors.domain.repositories.SensorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfiguration {

  @Autowired
  private AlertRepository alertRepository;

  @Autowired
  private SensorRepository sensorRepository;

  @Autowired
  private MeasurementRepository measurementRepository;

  @Autowired
  private SensorStatusHandlerInterface sensorStatusHandler;

  @Bean
  public SensorService sensorService() {
    return new SensorService(
            sensorStatusHandler,
            sensorRepository,
            alertRepository,
            measurementRepository
    );
  }

}
