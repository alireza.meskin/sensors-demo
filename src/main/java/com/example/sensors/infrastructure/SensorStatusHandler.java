package com.example.sensors.infrastructure;

import com.example.sensors.domain.SensorStatusHandlerInterface;
import com.example.sensors.domain.models.Measurement;
import com.example.sensors.domain.models.Sensor;
import com.example.sensors.domain.models.Sensor.Status;
import org.springframework.stereotype.Component;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import static com.example.sensors.domain.models.Sensor.Status.ALERT;
import static com.example.sensors.domain.models.Sensor.Status.OK;
import static com.example.sensors.domain.models.Sensor.Status.WARN;

@Component
public class SensorStatusHandler implements SensorStatusHandlerInterface {
  private final int THRESHOLD = 2000;

  private final ConcurrentHashMap<UUID, AtomicInteger> exceedsCount = new ConcurrentHashMap<>();
  private final ConcurrentHashMap<UUID, AtomicInteger> smallsCount = new ConcurrentHashMap<>();

  public Status newMeasurement(Sensor sensor, Measurement measurement) {
    if (sensor.getStatus() == OK)
      return processOKSensor(sensor, measurement);

    if (sensor.getStatus() == WARN)
      return processWarnSensor(sensor, measurement);

    return processAlertSensor(sensor, measurement);
  }

  private Status processOKSensor(Sensor sensor, Measurement measurement) {

    if (!exceedsThreshold(measurement))
      return OK;

    exceedsCount.put(sensor.getUuid(), new AtomicInteger(1));

    return WARN;
  }

  private Status processWarnSensor(Sensor sensor, Measurement measurement) {
    if (!exceedsThreshold(measurement)) {
      exceedsCount.put(sensor.getUuid(), new AtomicInteger(0));

      return OK;
    }

    int count = exceedsCount.computeIfAbsent(sensor.getUuid(), u -> new AtomicInteger(0)).incrementAndGet();

    if (count > 2) {
      return ALERT;
    }

    return WARN;
  }

  private Status processAlertSensor(Sensor sensor, Measurement measurement) {
    if (!exceedsThreshold(measurement)) {
      int count = smallsCount.computeIfAbsent(sensor.getUuid(), u -> new AtomicInteger(0)).incrementAndGet();

      if (count > 2) {
        return OK;
      }
    } else {
      smallsCount.put(sensor.getUuid(), new AtomicInteger(0));
    }

    return ALERT;
  }

  private boolean exceedsThreshold(Measurement measurement) {
    return measurement.getCo2() > THRESHOLD;
  }
}
