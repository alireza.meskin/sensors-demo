package com.example.sensors.infrastructure;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.example.sensors.domain.models.Measurement;
import com.example.sensors.domain.models.Sensor;
import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import static com.example.sensors.domain.models.Sensor.Status.ALERT;
import static com.example.sensors.domain.models.Sensor.Status.OK;
import static com.example.sensors.domain.models.Sensor.Status.WARN;

@RunWith(Parameterized.class)
public class SensorStatusHandlerTest extends TestCase {

  @Parameter(0)
  public Sensor sensor;

  @Parameter(1)
  public List<Integer> measurements;

  @Parameter(2)
  public Sensor.Status expectedStatus;

  @Test
  public void testNewMeasurement() {
    SensorStatusHandler handler = new SensorStatusHandler();
    measurements.forEach(m -> {
          Sensor.Status newStatus = handler.newMeasurement(sensor, new Measurement(sensor.getUuid(), m, LocalDateTime.now()));

          sensor.setStatus(newStatus);
        }
    );

    assertEquals(expectedStatus, sensor.getStatus());
  }

  @Parameters
  public static Collection<Object[]> provideMeasurements() {
    Object[][] data = new Object[][] {
        {
          new Sensor(UUID.randomUUID(), OK),
          Arrays.asList(2001, 2001, 2001),
          ALERT
        },
        {
            new Sensor(UUID.randomUUID(), ALERT),
            Arrays.asList(1000, 1000, 2001, 1000, 1020),
            ALERT
        },
        {
            new Sensor(UUID.randomUUID(), ALERT),
            Arrays.asList(1000, 2001, 1501, 1000, 1020),
            OK
        },
        {
            new Sensor(UUID.randomUUID(), WARN),
            Arrays.asList(2001, 2001, 1501, 1000, 1020),
            OK
        },
    };

    return Arrays.asList(data);
  }

}