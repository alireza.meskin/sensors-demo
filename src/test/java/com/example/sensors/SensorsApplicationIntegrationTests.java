package com.example.sensors;

import com.example.sensors.domain.SensorService;
import com.example.sensors.domain.models.Alert;
import com.example.sensors.domain.models.Sensor;
import com.example.sensors.domain.repositories.AlertRepository;
import com.example.sensors.domain.repositories.SensorRepository;
import com.example.sensors.infrastructure.dto.request.MeasurementDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Stream;

import static com.example.sensors.domain.models.Sensor.Status.*;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
@AutoConfigureTestDatabase
public class SensorsApplicationIntegrationTests {

  @Autowired
  private TestRestTemplate restTemplate;

  @Autowired
  private SensorService sensorService;

  @Autowired
  private SensorRepository sensorRepository;

  @Autowired
  private AlertRepository alertRepository;

  @Test
  public void test_notFoundForInvalidSensor() {

    ResponseEntity<String> response = this.restTemplate
            .getForEntity("/api/v1/sensors/{uuid}", String.class, "5d06c0d0-fc5f-4710-b21a-9b80b3e17b67");

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
  }

  @Test
  public void test_validStatusForSensor() {
    Sensor sensor = generateSensor(WARN);

    ResponseEntity<Map> response = this.restTemplate
            .getForEntity("/api/v1/sensors/{uuid}", Map.class, sensor.getUuid().toString());

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody().get("status")).isEqualTo(WARN.toString());
  }

  @Test
  public void test_getAlerts() {
    Sensor sensor = generateSensor(OK);

    Alert alert1 = new Alert();
    alert1.setId(100L);
    alert1.setSensorId(sensor.getUuid());
    alert1.setStartTime(LocalDateTime.now());
    alert1.setEndTime(LocalDateTime.now());

    alertRepository.save(alert1);

    Alert alert2 = new Alert();
    alert2.setId(200L);
    alert2.setSensorId(sensor.getUuid());
    alert2.setStartTime(LocalDateTime.now());
    alert2.setEndTime(LocalDateTime.now());

    alertRepository.save(alert2);

    ResponseEntity<List> response = this.restTemplate
            .getForEntity("/api/v1/sensors/{uuid}/alerts", List.class, sensor.getUuid().toString());

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).hasSize(2);
  }

  @Test
  public void test_submitMeasurement() {
    Sensor sensor = generateSensor(OK);

    Stream.of(1700, 2001, 2010, 2020, 2030)
            .map(co2 -> new MeasurementDto(co2, LocalDateTime.now()))
            .forEach(req ->
                    restTemplate.postForEntity("/api/v1/sensors/{uuid}/measurements", req, String.class, sensor.getUuid())
            );

    ResponseEntity<Map> response = this.restTemplate
            .getForEntity("/api/v1/sensors/{uuid}", Map.class, sensor.getUuid().toString());

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody().get("status")).isEqualTo(ALERT.toString());
  }

  @Test
  public void  test_getMetrics() {
    Sensor sensor = generateSensor(OK);

    Stream.of(1700, 2001, 2010, 2020, 2030, 2040, 1000, 1405, 2050, 1034, 1234, 14343)
            .map(co2 -> new MeasurementDto(co2, LocalDateTime.now()))
            .forEach(req ->
                    restTemplate.postForEntity("/api/v1/sensors/{uuid}/measurements", req, String.class, sensor.getUuid())
            );

    ResponseEntity<String> response = this.restTemplate
            .getForEntity("/api/v1/sensors/{uuid}/metrics", String.class, sensor.getUuid().toString());

    System.out.println("response = " + response);
  }

  private Sensor generateSensor(Sensor.Status status) {
    UUID uuid = UUID.randomUUID();
    Sensor sensor = new Sensor();
    sensor.setStatus(status);
    sensor.setUuid(uuid);

    return sensorRepository.save(sensor);
  }

}
