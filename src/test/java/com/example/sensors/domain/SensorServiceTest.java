package com.example.sensors.domain;

import com.example.sensors.domain.models.Alert;
import com.example.sensors.domain.models.Measurement;
import com.example.sensors.domain.models.Sensor;
import com.example.sensors.domain.repositories.AlertRepository;
import com.example.sensors.domain.repositories.MeasurementRepository;
import com.example.sensors.domain.repositories.SensorRepository;
import com.example.sensors.infrastructure.SensorStatusHandler;
import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.example.sensors.domain.models.Sensor.Status.OK;
import static com.example.sensors.domain.models.Sensor.Status.WARN;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SensorServiceTest extends TestCase {

  @Mock
  AlertRepository alertRepository;

  @Mock
  SensorRepository sensorRepository;

  @Mock
  MeasurementRepository measurementRepository;

  @Mock
  SensorStatusHandler sensorStatusHandler;

  @InjectMocks
  SensorService sensorService;

  @Test
  public void test_getSensorStatus() {
    UUID uuid = UUID.randomUUID();
    Sensor sensor = new Sensor();
    sensor.setUuid(uuid);
    sensor.setStatus(OK);
    when(sensorRepository.findById(uuid)).thenReturn(Optional.of(sensor));

    assertEquals(OK, sensorService.getSensorStatus(uuid));
  }

  @Test
  public void test_getMaximumLevelInLastDays() {
    UUID uuid = UUID.randomUUID();
    Sensor sensor = new Sensor();
    sensor.setUuid(uuid);
    sensor.setStatus(OK);

    LocalDateTime firstDate = LocalDate.now().minusDays(10).atStartOfDay();
    LocalDateTime lastDate  = LocalDate.now().atTime(LocalTime.MAX);
    when(measurementRepository.findMaxBetweenDates(uuid, firstDate, lastDate)).thenReturn(Optional.of(1300));

    assertThat(sensorService.getMaximumLevelInLastDays(uuid, 10)).hasValue(1300);
  }

  @Test
  public void test_getAverageLevelInLastDays() {
    UUID uuid = UUID.randomUUID();
    Sensor sensor = new Sensor();
    sensor.setUuid(uuid);
    sensor.setStatus(OK);

    LocalDateTime firstDate = LocalDate.now().minusDays(10).atStartOfDay();
    LocalDateTime lastDate  = LocalDate.now().atTime(LocalTime.MAX);

    when(measurementRepository.findAverageBetweenDates(uuid, firstDate, lastDate)).thenReturn(Optional.of(1350.0));

    assertThat(sensorService.getAverageLevelInLastDays(uuid, 10)).hasValue(1350.0);
  }

  @Test
  public void test_getSensorAlerts() {
    UUID uuid = UUID.randomUUID();
    Sensor sensor = new Sensor();
    sensor.setUuid(uuid);
    sensor.setStatus(OK);

    when(sensorRepository.findById(uuid)).thenReturn(Optional.of(sensor));
    when(alertRepository.findBySensorId(uuid)).thenReturn(Arrays.asList(new Alert()));

    assertThat(sensorService.getSensorAlerts(uuid)).isInstanceOf(List.class).hasSize(1);
  }

  @Test
  public void test_submitMeasurement() {
    UUID uuid = UUID.randomUUID();
    Sensor sensor = new Sensor();
    sensor.setUuid(uuid);
    sensor.setStatus(OK);

    LocalDateTime now = LocalDateTime.now();
    Measurement measurement = new Measurement(uuid, 2001, now);

    when(sensorRepository.findById(uuid)).thenReturn(Optional.of(sensor));
    when(measurementRepository.save(Mockito.any(Measurement.class))).thenReturn(measurement);
    when(sensorStatusHandler.newMeasurement(sensor, measurement)).thenReturn(WARN);
    when(sensorRepository.save(sensor)).thenReturn(sensor);

    sensorService.submitMeasurement(uuid,2001,now);
    assertEquals(WARN, sensor.getStatus());
  }
}