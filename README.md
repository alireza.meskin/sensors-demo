
It is a sample project which receives and processes co2 measurements from sensors and alerts when a co2 level reported by sensors reaches critical levels.

## Running 

```bash
./mvnw spring-boot:run
```

Getting a sensor status :

```bash
http get http://localhost:8080/api/v1/sensors/58cc88f7-a824-4f2d-97ba-0715b1a302f1
```

Sending some measurements for a sensor:

```bash
http POST http://localhost:8080/api/v1/sensors/58cc88f7-a824-4f2d-97ba-0715b1a302f1/measurements co2=1200 time=2019-09-06T20:16:05.334
http POST http://localhost:8080/api/v1/sensors/58cc88f7-a824-4f2d-97ba-0715b1a302f1/measurements co2=1800 time=2019-09-06T20:21:05.334
http POST http://localhost:8080/api/v1/sensors/58cc88f7-a824-4f2d-97ba-0715b1a302f1/measurements co2=1900 time=2019-09-06T20:22:05.334
http POST http://localhost:8080/api/v1/sensors/58cc88f7-a824-4f2d-97ba-0715b1a302f1/measurements co2=2100 time=2019-09-06T20:23:05.334
http POST http://localhost:8080/api/v1/sensors/58cc88f7-a824-4f2d-97ba-0715b1a302f1/measurements co2=2110 time=2019-09-06T20:24:05.334
http POST http://localhost:8080/api/v1/sensors/58cc88f7-a824-4f2d-97ba-0715b1a302f1/measurements co2=2120 time=2019-09-06T20:25:05.334

http get http://localhost:8080/api/v1/sensors/58cc88f7-a824-4f2d-97ba-0715b1a302f1
```

Getting metrics for a sensor :
```bash
http get http://localhost:8080/api/v1/sensors/58cc88f7-a824-4f2d-97ba-0715b1a302f1/metrics
```

Getting alerts of a sensor:
```bash
http POST http://localhost:8080/api/v1/sensors/58cc88f7-a824-4f2d-97ba-0715b1a302f1/measurements co2=1800 time=2019-09-06T20:30:05.334
http POST http://localhost:8080/api/v1/sensors/58cc88f7-a824-4f2d-97ba-0715b1a302f1/measurements co2=1600 time=2019-09-06T20:31:05.334
http POST http://localhost:8080/api/v1/sensors/58cc88f7-a824-4f2d-97ba-0715b1a302f1/measurements co2=1400 time=2019-09-06T20:32:05.334

http get http://localhost:8080/api/v1/sensors/58cc88f7-a824-4f2d-97ba-0715b1a302f1/alerts
```
